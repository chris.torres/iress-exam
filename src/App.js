import './App.css';
import Tabs from './components/Tabs';


function App() {
  return (
    <div className="container">
      <div className="content">
        <Tabs id="tabOne">
          <div label="Tincidunt">
            <h1>Donec Vitae</h1>
            <p>Ut tincidunt tincidunt erat. Fusce convallis metus id felis luctus adipiscing. 
              Vivamus quis mi. Nullam vel sem. Morbi ac felis.</p>

            <p>Maecenas vestibulum mollis diam.. Proin viverra, ligula sit amet ultrices semper, 
              ligula arcu tristique sapien, a accumsan nisi mauris ac eros. Suspendisse potenti. 
              In dui magna, posuere eget, vestibulum et, tempor auctor, justo.</p>
          </div>
          <div label="Sodales">
            <h1>Donec sodales sagittis</h1>
            <p>Maecenas vestibulum mollis diam.. Proin viverra, ligula sit amet ultrices semper, 
              ligula arcu tristique sapien, a accumsan nisi mauris ac eros. Suspendisse potenti. 
              In dui magna, posuere eget, vestibulum et, tempor auctor, justo.</p>
          </div>
          <div label="Lorem">
            <h1>Lorem ipsum dolor</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
              incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
              Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu 
              fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, 
              sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          </div>
        </Tabs>

        <Tabs id="tabTwo">
          <div label="Tincidunt2">
            <h1>Donec Vitae</h1>
            <p>Ut tincidunt tincidunt erat. Fusce convallis metus id felis luctus adipiscing. 
              Vivamus quis mi. Nullam vel sem. Morbi ac felis.</p>

            <p>Maecenas vestibulum mollis diam.. Proin viverra, ligula sit amet ultrices semper, 
              ligula arcu tristique sapien, a accumsan nisi mauris ac eros. Suspendisse potenti. 
              In dui magna, posuere eget, vestibulum et, tempor auctor, justo.</p>
          </div>
          <div label="Sodales2">
            <h1>Donec sodales sagittis</h1>
            <p>Maecenas vestibulum mollis diam.. Proin viverra, ligula sit amet ultrices semper, 
              ligula arcu tristique sapien, a accumsan nisi mauris ac eros. Suspendisse potenti. 
              In dui magna, posuere eget, vestibulum et, tempor auctor, justo.</p>
          </div>
        </Tabs>

        <Tabs id="tabThree">
          <div label="Tincidunt3">
            <h1>Donec Vitae</h1>
            <p>Ut tincidunt tincidunt erat. Fusce convallis metus id felis luctus adipiscing. 
              Vivamus quis mi. Nullam vel sem. Morbi ac felis.</p>

            <p>Maecenas vestibulum mollis diam.. Proin viverra, ligula sit amet ultrices semper, 
              ligula arcu tristique sapien, a accumsan nisi mauris ac eros. Suspendisse potenti. 
              In dui magna, posuere eget, vestibulum et, tempor auctor, justo.</p>
          </div>
          <div label="Sodales3">
            <h1>Donec sodales sagittis</h1>
            <p>Maecenas vestibulum mollis diam.. Proin viverra, ligula sit amet ultrices semper, 
              ligula arcu tristique sapien, a accumsan nisi mauris ac eros. Suspendisse potenti. 
              In dui magna, posuere eget, vestibulum et, tempor auctor, justo.</p>
          </div>
        </Tabs>
      </div>
      
      
    </div>
    
    
  );
}

export default App
