import { fireEvent, render, userEvent } from '@testing-library/react';
import App from './App';

describe('App', () => {
  test('render App component', () => {
    render(<App />)
  })
})

describe('Tabs', ()=> {
  test('check if Tabs is loaded and if configured correctly', () => {
    const {container} = render(<App />);
    
    
    const div = container.querySelector('.tabs');

    const expected = !!div
    expect(true).toEqual(expected)
  })

  test('check if next tab ok when clicked', () => {
    const { getByText } = render(<App />)
    const butt1 = getByText('Tincidunt').closest('button')
    const li1 = butt1.parentElement
    const butt2 = getByText('Sodales').closest('button')
    const li2 = butt2.parentElement
    fireEvent.click(butt1)
    expect(li1).toHaveClass("tab-list-active")
    fireEvent.click(butt2)
    expect(li2).toHaveClass("tab-list-active")
  })

  test('check if keydown events are working', () => {
    // const { getByText, container } = render(<App />)
    // const butt1 = getByText('Tincidunt').closest('button')
    // const li1 = butt1.parentElement
    // const butt2 = getByText('Sodales').closest('button')
    // const li2 = butt2.parentElement
    // const tab = container.querySelector('#tabOne')
    // fireEvent.click(butt1)
    // expect(li1).toHaveClass("tab-list-active")
    // tab.focus()
    // fireEvent.keyDown(document.activeElement, {key: 'ArrowRight'})
    // expect(li2).toHaveClass("tab-list-active")
  })
})

