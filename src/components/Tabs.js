import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Tab from './Tab';

class Tabs extends Component {
  static propTypes = {
    children: PropTypes.instanceOf(Array).isRequired,
  }

  constructor(props) {
    super(props)

    let activeLabel = (localStorage.getItem(`${this.props.id}`) ? 
      localStorage.getItem(`${this.props.id}`) : this.props.children[0].props.label)
    this.state = {
      activeTab: activeLabel,
    };
  }

  onClickTabItem = (tab) => {
    this.setState({ activeTab: tab });
    localStorage.setItem(`${this.props.id}`, tab)
  }

  activeGroupTab = (event) => {
    let lis = document.querySelectorAll(`div#${this.props.id} ol li button`)
    lis[0].focus()
  }

  triggerEvent = (event) => {
    let lis = document.querySelectorAll(`div#${this.props.id} ol li button`)
    let activeTab,
      index, 
      newIndex
    switch (event.key) {
      case 'ArrowRight':
        activeTab = document.querySelector(`div#${this.props.id} ol li.tab-list-active button`)
        index = Array.prototype.slice.call(lis).indexOf(activeTab)
        newIndex = (index === lis.length-1 ? index : index+1)
        this.onClickTabItem(lis[newIndex].outerText)
        break
      case 'ArrowLeft':
        activeTab = document.querySelector(`div#${this.props.id} ol li.tab-list-active button`)
        index = Array.prototype.slice.call(lis).indexOf(activeTab)
        newIndex = (index === 0 ? index : index-1)
        this.onClickTabItem(lis[newIndex].outerText)
        break
      case 'Home':
        this.onClickTabItem(lis[0].outerText)
        break
      case 'End':
        this.onClickTabItem(lis[lis.length-1].outerText)
        break
      default:
        // do nothing
        break
    }
    event.stopPropagation();
    event.preventDefault()
  }

  componentDidMount () {
    let el = document.querySelector(`div#${this.props.id}`)
 
    el.removeEventListener('keydown', this.triggerEvent)
    el.addEventListener('keydown', this.triggerEvent)
  }

  render() {
    const {
      onClickTabItem,
      activeGroupTab,
      props: {
        children,
        id,
      },
      state: {
        activeTab,
      },
    } = this;

    return (
      <div id={this.props.id} role="tab" className="tabs" onClick={activeGroupTab}>
        <ol className="tab-list">
          {children.map((child) => {
            const { label } = child.props

            return (
              <Tab
                activeTab={activeTab}
                key={label}
                label={label}
                onClick={onClickTabItem}
                parent={id}
              />
            );
          })}
        </ol>
        <div className="tab-content">
          {children.map((child) => {
            if (child.props.label !== activeTab) return undefined;
            return child.props.children
          })}
        </div>
      </div>
    );
  }

}

export default Tabs
