
# Iress exam - chris torres

#### View the [demo](http://iress-exam.s3-website.us-east-2.amazonaws.com)  

---


This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

  

## Available Scripts

  

In the project directory, you can run:

  

### `npm start`

  

Runs the app in the development mode.\

Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

  

The page will reload when you make changes.\

You may also see any lint errors in the console.

  

### `npm test`

  

Launches the test runner in the interactive watch mode.\

See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

  

### `npm run build`

  

Builds the app for production to the `build` folder.\

It correctly bundles React in production mode and optimizes the build for the best performance.

  

The build is minified and the filenames include the hashes.\

Your app is ready to be deployed!

  

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

  
## Components and how to use them

### Tabs

    src/components/Tabs.js
Tab is fully compliant with the  WAI ARAI specification. https://www.w3.org/WAI/ARIA/apg/patterns/tabpanel/. Tab component will retain their state after refresh.

|Prop name  |Type  |Required  |Description  |
|--|--|--|--|
|id  |String  |Yes  |Unique identifier for Tab  |

#### Usage

    <Tabs id="{Tab ID}"> // Tab
	    <div label={Tab Label}> // Tab label. Multiple tabs supported.
		    <h1>{Tab Title}</h1> // Tab title or header.
		    <p>{Content}</p> // Tab content.
		</div>
	</Tabs>






